module com.example.animalshelter {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.animalshelter to javafx.fxml;
    exports com.example.animalshelter;
}