package com.example.animalshelter.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DBConnection
{
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public DBConnection(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=sample","sa","1234");
            System.out.println("DATABASE READY");
        }catch (Exception e){
            e.getMessage();
        }
    }

    //@Override //lacks implement, but we dont have a matching interface yet
    public void closeConnection(){
        try{
            ps.close();
            con.close();
            System.out.println("CONNECTION CLOSED");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
